package com.tomjshore.termimage;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class RendererTest {

    private Renderer renderer;

    @BeforeEach
    public void setUp(){
        renderer = new Renderer();
    }

    @Test
    void testRenderMakesAStringWithTheCorrectCleanScreen(){
        //given
        boolean[] pixels = {true, true, true, true};
        int width = 2;
        int height = 2;

        String expectedStartOfString = "\n\n"; //Two new lines which will "clear" the terminal screen

        //when
        String result = renderer.render(pixels, width, height);

        //then
        assertTrue(result.startsWith(expectedStartOfString));
    }

    @Test
    void testRenderMakesAStringWithAllBlackPixelsWhenPixelArrayIsAllTrue(){
        //given
        boolean[] pixels = {true, true, true, true};
        int width = 2;
        int height = 2;
        String expectedString = """
                
                
                \u2588\u2588\u2588\u2588
                \u2588\u2588\u2588\u2588
                """;//2 newlines, 4 blocks, 1 newline, 4 blocks

        //when
        String result = renderer.render(pixels, width, height);

        //then
        assertEquals(expectedString, result);
    }

    @Test
    void testRenderMakesAStringWithAllWhitePixelsWhenPixelArrayIsAllTrue(){
        //given
        boolean[] pixels = {false, false, false, false};
        int width = 2;
        int height = 2;
        String expectedString = """
                
                
                \s\s\s\s
                \s\s\s\s
                """;//2 newlines, 4 spaces, 1 newline, 4 spaces

        //when
        String result = renderer.render(pixels, width, height);

        //then
        assertEquals(expectedString, result);
    }

    @Test
    void testRenderMakesStringInACheckerBoardWhenPixelsAreFalseTrueTrueFalse(){
        //given
        boolean[] pixels = {false, true, true, false};
        int width = 2;
        int height = 2;
        String expectedString = """
                
                
                \s\s\u2588\u2588
                \u2588\u2588\s\s
                """;//2 newlines, 2 spaces, 2 blocks, 1 newline, 2 blocks, 2 spaces

        //when
        String result = renderer.render(pixels, width, height);

        //then
        assertEquals(expectedString, result);
    }
}
