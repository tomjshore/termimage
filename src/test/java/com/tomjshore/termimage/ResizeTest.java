package com.tomjshore.termimage;

import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ResizeTest {

    @Test
    void testResizeChangesSizeOfTheImageToCorrectWidthAndHeight() throws IOException, ImageReadException {
        //given
        int width = 80;
        int height = 10;
        InputStream inputStream = new ClassPathResource("images/linux-tshirt.jpg").getInputStream();
        BufferedImage image = Imaging.getBufferedImage(inputStream);

        //when
        BufferedImage result = Resize.resize(image, width, height);

        //then
        assertEquals(width, result.getWidth());
        assertEquals(height, result.getHeight());
    }
}
