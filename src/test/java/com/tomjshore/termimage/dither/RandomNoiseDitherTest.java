package com.tomjshore.termimage.dither;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class RandomNoiseDitherTest {

    @Mock
    private Random random;

    @Mock
    private BufferedImage image;


    @InjectMocks
    private RandomNoiseDither dither;

    @Test
    void testDitherReturnsAllTrueWhenPixelsAreLightButRandomIs1(){
        //given
        given(random.nextInt(255)).willReturn(1);
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.LIGHT_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {true, true, true, true};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    @Test
    void testDitherReturnsAllFalseWhenPixelsAreLightButRandomIs255(){
        //given
        given(random.nextInt(255)).willReturn(255);
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.LIGHT_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {false, false, false, false};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    @Test
    void testDitherReturnsAllTrueWhenPixelsAreDarkButRandomIs1(){
        //given
        given(random.nextInt(255)).willReturn(1);
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.DARK_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {true, true, true, true};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    @Test
    void testDitherReturnsAllFalseWhenPixelsAreDarkButRandomIs255(){
        //given
        given(random.nextInt(255)).willReturn(255);
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.DARK_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {false, false, false, false};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }
}
