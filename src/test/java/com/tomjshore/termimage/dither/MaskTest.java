package com.tomjshore.termimage.dither;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MaskTest {

    private Mask mask;

    @BeforeEach
    void setUp(){
        mask = new Mask(List.of(64, 128, 128, 0));
    }

    @Test
    void testGetDarkestReturns64WhenXandYis00(){
        //given
        int x = 0;
        int y = 0;

        //when
        int result = mask.getDarkestTolerance(x, y);

        //then
        assertEquals(64, result);
    }

    @Test
    void testGetDarkestReturns128WhenXandYis10(){
        //given
        int x = 1;
        int y = 0;

        //when
        int result = mask.getDarkestTolerance(x, y);

        //then
        assertEquals(128, result);
    }

    @Test
    void testGetDarkestReturns0WhenXandYis11(){
        //given
        int x = 1;
        int y = 1;

        //when
        int result = mask.getDarkestTolerance(x, y);

        //then
        assertEquals(0, result);
    }

    @Test
    void testGetDarkestReturns64WhenXandYis20(){
        //given
        int x = 2;
        int y = 0;

        //when
        int result = mask.getDarkestTolerance(x, y);

        //then
        assertEquals(64, result);
    }

    @Test
    void testGetDarkestWrapsCorrectly(){
        //given
        List<Integer> expected = List.of(64, 128, 128, 0, 64, 128, 128, 0, 64, 128, 128, 0, 64, 128, 128, 0, 64, 128, 128, 0);
        List<Integer> results = new ArrayList<>();

        //when
        for(int t =0 ; t < 5; t++) {
            for (int y = 0; y < 2; y++) {
                for (int x = 0; x < 2; x++) {
                    results.add(mask.getDarkestTolerance(x, y));
                }
            }
        }

        //then
        assertArrayEquals(expected.toArray(), results.toArray());
    }

}
