package com.tomjshore.termimage.dither;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.awt.image.BufferedImage;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class BayerFilterDitherTest {

    @Mock
    private BufferedImage image;

    private Dither dither;

    @BeforeEach
    void setUp(){
        dither = new BayerFilterDither();
    }

    @Test
    void testDitherReturnsCheckerBoardArrayWhenImageIsGray(){
        //given
        Color gray = new Color(127,127,127,127);
        given(image.getRGB(anyInt(), anyInt())).willReturn(gray.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {true, false, false, true};


        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }
}
