package com.tomjshore.termimage.dither;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.awt.image.BufferedImage;


import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class FloydSteinbergDitherTest {

    private Dither dither;

    @BeforeEach
    void setUp(){
        dither = new FloydSteinbergDither();
    }


    @Test
    void testDither()  {
        //given
        BufferedImage image = new BufferedImage(4,4, BufferedImage.TYPE_INT_RGB);
        image.setRGB(0,0, makeRgb(153)); // mod1 NA mod2 NA | error -102 | total 153
        image.setRGB(1,0, makeRgb(102)); // mod1 -51 mod2 NA | error 51 | total 51
        image.setRGB(2,0, makeRgb(102)); // mod1 25 mod2 NA | error 127 | total 127
        image.setRGB(3,0, makeRgb(153)); // mod1 63 mod2 NA | error -39 | total 216

        image.setRGB(0,1, makeRgb(153)); // mod1 NA mod2 -51 | error 102 | total 102
        image.setRGB(1,1, makeRgb(102)); // mod1 51 mod2 25 | error -77 | total 178
        image.setRGB(2,1, makeRgb(229)); // mod1 -38 mod2 63 | error -1 | total 254
        image.setRGB(3,1, makeRgb(51));  // mod1 0 mod2 -19 | error 32 | total 32

        image.setRGB(0,2, makeRgb(102)); // mod1 NA mod2 51 | error -102 | total 153
        image.setRGB(1,2, makeRgb(153)); // mod1 -51 mod2 -38 | error 64 | total 64
        image.setRGB(2,2, makeRgb(25));  // mod1 32 mod2 0 | error 57 | total 57
        image.setRGB(3,2,makeRgb(178));  // mod1 28 mod2 16 | error -33 | total 222

        image.setRGB(0,3, makeRgb(204)); // mod1 NA mod2 -51 | error -102 | total 153
        image.setRGB(1,3, makeRgb(102)); // mod1 -51 mod2 32 | error 83 | total 83
        image.setRGB(2,3, makeRgb(127)); // mod1 41 mod2 28 | error 193 | total 193
        image.setRGB(3,3, makeRgb(76));  // mod1 98 mod2 -16 | error NA | total 158

        boolean[] expectedPixels = {
                true, false, false, true,
                false, true, true, false,
                true, false, false, true,
                true, false, true, false
        };

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    private int makeRgb(int lightness){
        Color color = new Color(lightness, lightness, lightness, 255);
        return color.getRGB();
    }
}
