package com.tomjshore.termimage.dither;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.awt.*;
import java.awt.image.BufferedImage;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class NoNoiseDitherTest {

    @Mock
    private BufferedImage image;

    private Dither dither;

    @BeforeEach
    void setUp(){
        dither = new NoNoiseDither();
    }

    @Test
    void testDitherReturnsAllTrueArrayWhenImageIsVeryDark(){
        //given
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.DARK_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {false, false, false, false};


        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    @Test
    void testDitherReturnsAllFalseArrayWhenImageIsVeryLight(){
        //given
        given(image.getRGB(anyInt(), anyInt())).willReturn(Color.LIGHT_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {true, true, true, true};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }

    @Test
    void testDitherReturnsAllCheckerBoardArrayWhenImageIsVeryDarkVeryLightVeryLightVeryDark(){
        //given
        given(image.getRGB(anyInt(), anyInt())).willReturn(
                Color.DARK_GRAY.getRGB(),
                Color.LIGHT_GRAY.getRGB(),
                Color.LIGHT_GRAY.getRGB(),
                Color.DARK_GRAY.getRGB());
        given(image.getWidth()).willReturn(2);
        given(image.getHeight()).willReturn(2);

        boolean[] expectedPixels = {false, true, true, false};

        //when
        boolean[] result = dither.dither(image);

        //then
        assertArrayEquals(expectedPixels,result);
    }
}
