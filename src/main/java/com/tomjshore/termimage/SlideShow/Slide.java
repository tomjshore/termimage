package com.tomjshore.termimage.SlideShow;

import com.tomjshore.termimage.dither.Dither;

import java.awt.image.BufferedImage;


public class Slide {

    private Dither dither;
    private BufferedImage image;

    public Slide(Dither dither, BufferedImage image){
        this.dither = dither;
        this.image = image;
    }

    public boolean[] getPixels(){
        return dither.dither(image);
    }
}
