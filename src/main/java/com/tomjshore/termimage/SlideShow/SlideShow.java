package com.tomjshore.termimage.SlideShow;

import com.tomjshore.termimage.Renderer;

import java.io.Console;
import java.io.IOException;
import java.util.List;

public class SlideShow {

    private int slideNumber = 0;
    private List<Slide> slides;
    private Renderer renderer;
    private int width;
    private int height;
    private Console console;

    public SlideShow(List<Slide> slides, Console console, Renderer renderer, int width, int height){
        this.slides = slides;
        this.renderer = renderer;
        this.width = width;
        this.height = height;
        this.console = console;
    }
    public void start(){
        renderSlide();
    }

    private void renderSlide(){
        Slide slide = this.slides.get(this.slideNumber);
        boolean[] pixels = slide.getPixels();
        String image = renderer.render(pixels, width, height);
        System.out.print(image);
        try {
            char c = (char) console.reader().read();
            if(c == 'l'){
                nextSlide();
            }
            if(c == 'h'){
                lastSlide();
            }
            if(c == 'q'){
                System.exit(0);
            }
        } catch (IOException e) {
            System.exit(0);
        }
        renderSlide();
    }

    private void nextSlide(){
        slideNumber++;
        if(slideNumber > slides.size() -1){
            slideNumber = slides.size() -1;
        }

    }

    private void lastSlide(){
        slideNumber--;
        if(slideNumber < 0){
            slideNumber = 0;
        }
    }
}
