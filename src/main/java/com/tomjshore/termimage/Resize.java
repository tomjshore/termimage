package com.tomjshore.termimage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Resize {

    /**
     * resizes the image will lose its aspect ratio
     * @param image to resize
     * @param width to resize to
     * @param height to resize to
     * @return copy of the resized image.
     */
    public static BufferedImage resize(BufferedImage image, int width, int height){
        BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = resizedImage.createGraphics();
        graphics2D.drawImage(image, 0,0, width, height, null);
        graphics2D.dispose();
        return resizedImage;
    }

}
