package com.tomjshore.termimage;

import com.tomjshore.termimage.SlideShow.Slide;
import com.tomjshore.termimage.SlideShow.SlideShow;
import com.tomjshore.termimage.dither.*;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.util.List;


@Component
public class CommandLineStart implements CommandLineRunner {

    private Renderer renderer;

    private NoNoiseDither noNoiseDither;
    private RandomNoiseDither randomNoiseDither;
    private BayerFilterDither bayerFilterDither;
    private FloydSteinbergDither floydSteinbergDither;

    private CommandLineStart(Renderer renderer,
                             NoNoiseDither noNoiseDither,
                             RandomNoiseDither randomNoiseDither,
                             BayerFilterDither bayerFilterDither,
                             FloydSteinbergDither floydSteinbergDither){
        this.renderer = renderer;
        this.noNoiseDither = noNoiseDither;
        this.randomNoiseDither = randomNoiseDither;
        this.bayerFilterDither = bayerFilterDither;
        this.floydSteinbergDither = floydSteinbergDither;
    }

    @Override
    public void run(String... args) throws Exception {

        int height = 139;
        int width = 630 / 2;

        BufferedImage me = makeImage(new File("/home/tom/slides/me.jpg"), width, height);
        BufferedImage lion = makeImage(new File("/home/tom/slides/lion.jpg"), width, height);
        BufferedImage boat = makeImage(new File("/home/tom/slides/boat.jpg"), width, height);

        List<Slide> slides = List.of(
                new Slide(noNoiseDither, me),
                new Slide(noNoiseDither, lion),
                new Slide(noNoiseDither, boat),
                new Slide(randomNoiseDither, me),
                new Slide(randomNoiseDither, lion),
                new Slide(randomNoiseDither, boat),
                new Slide(bayerFilterDither, me),
                new Slide(bayerFilterDither, lion),
                new Slide(bayerFilterDither, boat),
                new Slide(floydSteinbergDither, makeImage(new File("/home/tom/slides/me.jpg"), width, height)),
                new Slide(floydSteinbergDither, makeImage(new File("/home/tom/slides/lion.jpg"), width, height)),
                new Slide(floydSteinbergDither, makeImage(new File("/home/tom/slides/boat.jpg"), width, height))
        );

        SlideShow slideShow = new SlideShow(slides, System.console(),renderer,width, height);
        slideShow.start();

    }

    private BufferedImage makeImage(File file, int width, int height) throws IOException, ImageReadException {
        BufferedImage bufferedImage = Imaging.getBufferedImage(file);
        return Resize.resize(bufferedImage,width, height);
    }
}
