package com.tomjshore.termimage;

import org.springframework.stereotype.Component;

/**
 * Renders "pixels" with is a array of booleans to a String ready for the console.
 */
@Component
public class Renderer {

    private static final String WHITE_PIXEL = "\u2588\u2588";
    private static final String BLACK_PIXEL = "  ";

    /**
     * Renders the correct chars from the pixels
     * @param pixels the pixels to render
     * @param width of the terminal screen
     * @param height of the terminal screen
     * @return String ready to print to the terminal
     */
    public String render(boolean[] pixels, int width, int height) {
        StringBuilder stringBuilder = new StringBuilder(calculateAmountOfChars(width, height));
        addClearScreen(stringBuilder, height);

        int index = 0;
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                addPixel(stringBuilder, pixels, index);
                index++;
            }
            stringBuilder.append('\n');
        }
        return stringBuilder.toString();
    }

    private int calculateAmountOfChars(int width, int height){
        //this works because every pixels is two chars wide so (width*2)
        //and because we need to clear the screen and then add lines plus the lines for the newlines (height*4)
        return (width*2) + (height*4);
    }

    private void addClearScreen(StringBuilder stringBuilder, int height){
        stringBuilder.append("\n".repeat(height));
    }

    private void addPixel(StringBuilder stringBuilder, boolean[] pixels, int pixelIndex){
        if(pixels[pixelIndex]){
            stringBuilder.append(WHITE_PIXEL);
        }else {
            stringBuilder.append(BLACK_PIXEL);
        }
    }
}
