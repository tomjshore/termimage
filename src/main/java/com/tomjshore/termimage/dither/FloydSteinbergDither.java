package com.tomjshore.termimage.dither;

import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;

@Component("FloydSteinbergDither")
public class FloydSteinbergDither extends BaseDither {

    @Override
    public boolean[] dither(BufferedImage image) {
        BufferedImage copy = copyImage(image);
        return super.dither(copy);
    }

    @Override
    boolean isPixelDark(int x, int y, int darkness, BufferedImage image) {
        boolean isDark = darkness >= BaseDither.HalfDark;

        int errorDiffusion = calculateErrorDiffusion(isDark, darkness);

        tryToMakePixelDarker(image, x + 1, y, errorDiffusion);
        tryToMakePixelDarker(image, x, y + 1, errorDiffusion);

        return isDark;
    }

    private BufferedImage copyImage(BufferedImage image){
        return new BufferedImage(image.getColorModel(), image.copyData(null), image.isAlphaPremultiplied(), null);
    }

    private int calculateErrorDiffusion(boolean isDark, int darkness){
        int errorDiffusion = darkness;
        if(isDark){
            errorDiffusion = darkness - BaseDither.FullyDark;

        }
        return errorDiffusion / 2;
    }

    private void tryToMakePixelDarker(BufferedImage image, int x, int y, int darknessToAdd){
        if(x < image.getWidth() && y < image.getHeight()){
            Color oldColor = new Color(image.getRGB(x, y));
            int darkness = oldColor.getRed() + darknessToAdd;
            if (darkness < BaseDither.FullyLight) {
                darkness = BaseDither.FullyLight;
            }
            if (darkness > BaseDither.FullyDark) {
                darkness = BaseDither.FullyDark;
            }
            Color newColor = new Color(darkness, darkness, darkness, 255);
            image.setRGB(x, y, newColor.getRGB());
        }
    }
}
