package com.tomjshore.termimage.dither;


import java.util.ArrayList;
import java.util.List;

public class Mask {

    private final List<List<Integer>> values;
    public Mask(List<Integer> values) {
        int wrapIndex = values.size() / 2;
        this.values = new ArrayList<>(wrapIndex);
        int index = 0;
        for(int y = 0; y < wrapIndex; y++){
            List<Integer> xValues = new ArrayList<>(wrapIndex);
            this.values.add(y, xValues);
            for(int x = 0; x < wrapIndex; x++){
                xValues.add(x, values.get(index));
                index++;
            }
        }

    }


    public int getDarkestTolerance(int x, int y) {
        List<Integer> row = getElementWithWrapAround(this.values, y);
        return getElementWithWrapAround(row, x);

    }

    private <T> T getElementWithWrapAround(List<T> list, int index){
        if(index >= list.size()){
            int mod = (list.size() + (index % list.size())) % list.size();
            return list.get(mod);
        }
        return list.get(index);
    }
}
