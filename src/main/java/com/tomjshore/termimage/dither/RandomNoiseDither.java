package com.tomjshore.termimage.dither;

import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.util.Random;

@Component("RandomNoiseDither")
public class RandomNoiseDither extends BaseDither implements Dither{

    private final Random random;

    public RandomNoiseDither(Random random){
        this.random = random;
    }

    @Override
    boolean isPixelDark(int x, int y, int darkness, BufferedImage image) {
        return darkness >= random.nextInt(BaseDither.FullyDark);
    }
}
