package com.tomjshore.termimage.dither;

import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;
import java.util.List;


@Component("BayerFilterDither")
public class BayerFilterDither extends BaseDither implements Dither{
    private final Mask mask;

    public BayerFilterDither(){
        this.mask = new Mask(List.of(64, 128, 128, 0));
    }

    @Override
    boolean isPixelDark(int x, int y, int darkness, BufferedImage image) {
        return darkness >= mask.getDarkestTolerance(x, y);
    }
}
