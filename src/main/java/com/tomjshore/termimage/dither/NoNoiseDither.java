package com.tomjshore.termimage.dither;

import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;

@Component("NoNoiseDither")
public class NoNoiseDither extends BaseDither implements Dither{

    @Override
    boolean isPixelDark(int x, int y, int darkness, BufferedImage image) {
        return darkness >= BaseDither.HalfDark;
    }
}
