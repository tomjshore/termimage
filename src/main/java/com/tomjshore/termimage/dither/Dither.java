package com.tomjshore.termimage.dither;

import java.awt.image.BufferedImage;

public interface Dither {

    boolean[] dither(BufferedImage image);
}
