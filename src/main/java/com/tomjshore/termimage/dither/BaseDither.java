package com.tomjshore.termimage.dither;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class BaseDither implements Dither {

    protected static int FullyDark = 255;
    protected static int HalfDark = 128;
    protected static int FullyLight = 0;

    @Override
    public boolean[] dither(BufferedImage image) {
        int width = image.getWidth();
        int height = image.getHeight();
        boolean[] pixels = new boolean[width * height];

        int index = 0;
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                Color color = new Color(image.getRGB(x, y));
                pixels[index] = isPixelDark(x, y, color.getRed(), image);
                index++;
            }
        }

        return pixels;
    }


    abstract boolean isPixelDark(int x, int y, int darkness, BufferedImage image);
}
